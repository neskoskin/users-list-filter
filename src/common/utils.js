const toLowerCase = (param) => param.toLocaleLowerCase();

const sortByParams = (data, dir) => {
    // asc
    if (dir === 'asc') {
        return [...data].sort((a, b) => parseFloat(a.age) - parseFloat(b.age));
    }
    // desc
    return [...data].sort((a, b) => parseFloat(b.age) - parseFloat(a.age));
}
const filterByTerm = (data, searchTerm) => {
    return data.filter(user => {
        const { firstname, lastname } = user;
        const term = toLowerCase(searchTerm.trim());
        const firstName = toLowerCase(firstname);
        const lastName = toLowerCase(lastname);
        return firstName.includes(term) || lastName.includes(term);
    })
}

export {
    toLowerCase,
    sortByParams,
    filterByTerm
};