import React, { useRef } from 'react';
import { CSSTransition } from 'react-transition-group';

function NoResults() {
    const nodeRef = useRef(null);
    return (
        <div className="user-filter__no-users-found">
            <CSSTransition in={true} nodeRef={nodeRef} timeout={1000} classNames="fade" appear={true}>
                <div ref={nodeRef}>
                    No results
                </div>
            </CSSTransition>
        </div>
    );
}
export default NoResults;