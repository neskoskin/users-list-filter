import React, { useState } from 'react';

function UserListItem({ item }) {
    const [userDetailsExpanded, toggleUserDetails] = useState(false);

    const triggerUserDetails = () => toggleUserDetails(!userDetailsExpanded);

    const userDetails = userDetailsExpanded && <h2>Contact information: {item.email}</h2>;

    return (
        <li>
            <article className="user">
                <button className="user__trigger" onClick={triggerUserDetails} >
                    <div className="user__image"><img src={item.img} alt={'user'} /></div>
                    <div className="user__info">
                        <h2>{item.firstname} {item.lastname}</h2>
                        <p className="user__age">{item.age}</p>
                    </div>
                </button>
                <div className="user__extra-info">
                    {userDetails}
                </div>
            </article>
        </li>
    );
}
export default UserListItem;