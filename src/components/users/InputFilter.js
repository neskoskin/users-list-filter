import React from 'react';
function InputFilter({ searchTerm, setSearchTerm }) {
  const handleInputChange = event => {
    const inputVal = event.target.value;
    setSearchTerm(inputVal);
  };
  return (
    <div>
      <label htmlFor="input-field">Filter Users by Name</label>
      <input
        type="text"
        placeholder="Search"
        value={searchTerm}
        onChange={handleInputChange}
      />
    </div>
  );
}
export default InputFilter;