import React, { useState, useEffect, useMemo } from 'react';
import InputFilter from './InputFilter';
import UserListItem from './UserListItem';
import NoResults from './NoResults';
import { users } from '../../mock/users';
import { toLowerCase, sortByParams, filterByTerm } from '../../common/utils';
import '../../styles/modules/users.scss';

function UserList() {

  const [searchTerm, setSearchTerm] = useState("");
  const [usersInitialData, setUsers] = useState([]);

  useEffect(() => {
    const extendedUsersData = users.reduce((acc, value) => {
      const { firstname, lastname } = value;
      acc.push({ ...value, email: `${toLowerCase(lastname)}.${toLowerCase(firstname)}@gmail.com` });
      return acc;
    }, []);
    setUsers(extendedUsersData);
  }, []);
  const sortUsers = () => {
    const sortedRes = sortByParams(usersInitialData, 'asc');
    setUsers(sortedRes);
  };
  const initialList = useMemo(() => usersInitialData.map(item => (<UserListItem item={item} key={item.id} />)), [usersInitialData]);
  const filteredList = filterByTerm(usersInitialData, searchTerm).map(item => (<UserListItem item={item} key={item.id} />));

  const userData = searchTerm.length > 2 ? (filteredList.length ? filteredList : <NoResults />) : initialList;

  return (
    <div className="codeflair-assignment">
      <section className="user-filter">
        <InputFilter searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
        <button className="filter-button" onClick={sortUsers}>Sort by age</button>
      </section>
      <div className="user-list">
        <ul>
          {userData}
        </ul>
      </div>

      {searchTerm.length > 2 && userData.length && `Records found: ${userData.length}`}
    </div>
  );
}

export default UserList;